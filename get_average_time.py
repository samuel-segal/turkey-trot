import csv

tot_minutes = 0

with open('result.csv') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        #Accounts for discrepancy in data
        city = row[0]

        #Doesn't count the header
        if city == 'City':
            continue
        
        time = row[1]
        if city == 'Cedar Rapids':
            time = time[3:]
        ismale = row[3]=='Male'


        minutes = 0
        digit1 = time.split(':')[0]
        if(digit1=='1'):
            minutes = 60+int(time.split(':')[1])
        else:
            minutes = int(time.split(':')[0])
        tot_minutes += minutes
print('Total Minutes: '+str(tot_minutes))
