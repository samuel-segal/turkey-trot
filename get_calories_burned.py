import csv

total_calories = 0

with open('result.csv') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        #Accounts for discrepancy in data
        city = row[0]

        #Doesn't count the header
        if city == 'City':
            continue
        
        time = row[1]
        if city == 'Cedar Rapids':
            time = time[3:]
        ismale = row[3]=='Male'


        minutes = 0
        digit1 = time.split(':')[0]
        if(digit1=='1'):
            minutes = 60+int(time.split(':')[1])
        else:
            minutes = int(time.split(':')[0])

        
        calories = 0
        #This can be implemented more efficiently, but it's not for the sake of readability
        if ismale:
            if minutes < 15:
                calories = 452
            elif minutes < 20:
                calories = 406
            elif minutes < 30:
                calories = 467
            elif minutes < 40:
                calories = 527
            else:
                calories = 572
            
        else:
            if minutes < 15:
                calories = 339
            elif minutes < 20:
                calories = 305
            elif minutes < 30:
                calories = 350
            elif minutes < 40:
                calories = 395
            else:
                calories = 429

        print(row[3]+' racer burned '+str(calories)+' calories in '+str(minutes)+' minutes')

        total_calories += calories

        
print('Total calories burned: '+str(total_calories))
