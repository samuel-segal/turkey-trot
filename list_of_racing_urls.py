from functools import lru_cache
import urllib.request

from bs4 import BeautifulSoup


turkey_tradition = 'https://americanturkeytradition.com/'
headers={'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'} 

#Unless if you consider the HTTP calls and printouts, this function has no side effects, so caching it is alright
@lru_cache
def get_urls():
    req = urllib.request.Request(turkey_tradition, None, headers)
    response =  urllib.request.urlopen(req)
    home_html = response.read()

    home_soup = BeautifulSoup(home_html, 'html.parser')
    home_spans = home_soup.find_all('span',style='color: #f26a25;')


    out = dict()

    for span in home_spans:
        local_href = span.parent.get('href')
        print(local_href)
        try:
            loc_req = urllib.request.Request(local_href, None, headers)
            loc_res = urllib.request.urlopen(loc_req)
            loc_html = loc_res.read()

            loc_soup = BeautifulSoup(loc_html,'html.parser')
            result = loc_soup.find_all('a',style='color: #ff6600;')

            for ele in result:
                if ele.text == '2019 Results':
                   out[local_href] = ele.get('href')
            
        except:
            if type(local_href)==str:
                print('Issue with site: '+local_href)

    return out

if __name__ == '__main__':
    get_urls()
